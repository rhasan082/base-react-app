Features:
1. Support for Android, iOS, Web: using expo
2. Added static type checker - Flow
3. Based on Firebase
4. Plugins used
    - expo
    - firebase
    - necolas/react-native-web
    - react-navigation
    - react-native-elements